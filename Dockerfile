FROM php:7.2-apache
##instalacion y actualizacion
RUN apt-get update && apt-get install -y \
&& docker-php-ext-install mysqli pdo pdo_mysql
##rescribir carpetas
RUN a2enmod rewrite
##permiso sobre la carpeta
RUN chmod 777 -R -c /var/www